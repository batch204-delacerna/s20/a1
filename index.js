console.log("Hello World");

let number = Number(prompt("Please input a number:"));


console.log("The number you provided is " + number);
for (var i = number; i >= 0; i--) 
{
	if (i % 10 === 0 && i !== 50)
	{
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	else if (i % 5 ===0 && i !== 50) 
	{
		console.log(i);
	}

	if (i <= 50) 
	{
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}

	
}


//8. Create a variable that will contain the string supercalifragilisticexpialidocious.
let word = "supercalifragilisticexpialidocious";
let wordConsonants = "";

for (var i = 0; i < word.length; i++) 
{
	if (word[i] == "a" ||
		word[i] == "e" ||
		word[i] == "i" ||
		word[i] == "o" ||
		word[i] == "u") 
	{

		continue;
	}

	else
	{
		wordConsonants = wordConsonants + word[i];
	}
	
}

console.log(word);
console.log(wordConsonants);